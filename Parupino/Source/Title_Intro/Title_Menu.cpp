//=================================================================================================
//
//	タイトル メニュ
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Title_Menu.h"

//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	const float		Title_MenuState::MENU_X = 400;
	const float		Title_MenuState::MENU_Y_0 = 600;
	const float		Title_MenuState::MENU_Y_1 = MENU_Y_0 + PITCH_Y;
	const float		Title_MenuState::CURSOR_X = Title_MenuState::MENU_X - 110;
	const float		Title_MenuState::CURSOR_Y = Title_MenuState::MENU_Y_0 + 15;
	const float		Title_MenuState::PITCH_Y = 120;
	const float		Title_MenuState::EXPLAIN_X = 480;
	const float		Title_MenuState::EXPLAIN_Y = 840 + 5;


}	//namespace GAME

