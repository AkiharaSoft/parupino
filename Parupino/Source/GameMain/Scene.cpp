//=================================================================================================
//
// シーン
//		解放と確保を伴う状態遷移
//
//=================================================================================================

//-------------------------------------------------------------------------------------------------
// ヘッダファイルのインクルード
//-------------------------------------------------------------------------------------------------
#include "Scene.h"

//状態遷移先
#include "../Title_Intro/Title_Intro.h"
#include "../Introduction/Introduction.h"

#include "../Introduction/Intro_Img.h"

#include "../CharaSele/CharaSele.h"
#include "../FtgMain/FtgMain.h"
#include "../Training/Training.h"
#include "../Demo/DemoMain.h"
#include "../Result/Result.h"


//-------------------------------------------------------------------------------------------------
// 定義
//-------------------------------------------------------------------------------------------------
namespace GAME
{
	//------------------------------------------------------------------

	Scene::Scene ()
		: m_bTransit ( F )
	{
	}

	Scene::~Scene ()
	{
	}

	//------------------------------------------------------------------
	SceneManager::SceneManager()
	{
		//格闘部分共通パラメータシングルトン生成
		G_Ftg::Create ();


		//最初のシーンを設定ファイルから取得する
		m_pParam = make_shared < Param > ();	//パラメータ
		GameSettingFile stgs = m_pParam->GetGameSetting ();
		START_MODE startMode = stgs.GetStartMode ();

#if 0
		//test
		startMode = START_TITLE_INTRO;
//		startMode = START_INTRO;
//		startMode = START_CHARA_SELE;
//		startMode = START_BATTLE;
//		startMode = START_RESULT;
//		startMode = START_DEMO;
//		startMode = START_TRAINING;
//		startMode = TEST_VOID;
#endif // 0


		//開始シーンの選択
		P_GameScene pScene = nullptr;
		
		switch ( startMode )
		{
		case START_TITLE_INTRO:
			//タイトル イントロから開始
			pScene = make_shared < Title_Intro > ();
			break;

		case START_INTRO:


			//イントロから開始
//			pScene = make_shared < Introduction > ();
			pScene = make_shared < Intro_Img > ();

			
			break;

		case START_CHARA_SELE:
			//キャラセレから開始
//			pScene = make_shared < CharaSelect > ();
			break;

		case START_BATTLE:
			//バトルから開始
			pScene = make_shared < FtgMain > ();
			break;

		case START_RESULT:
			//リザルトから開始
			pScene = make_shared < Result > ();
			break;

		case START_TRAINING:
			//トレーニングから開始
			pScene = make_shared < Training > ();
			break;

		case START_DEMO:
			//デモから開始
			pScene = make_shared < Title_Intro > ();
			break;

		case TEST_VOID:
			//テスト：空のシーン
			pScene = make_shared < TestScene > ();
			break;
		}

		//パラメータの設定
		pScene->SetpParam ( m_pParam );
		pScene->ParamInit ();
//		pScene->Load ();

		//シーンの設定
		SetScene ( pScene );

#if 0
		//フェード
		m_fade = make_shared < FadeRect > ();
//		m_fade->SetBlackIn ( 60 );
		AddpTask ( m_fade );

#endif // 0
	}

	SceneManager::~SceneManager()
	{
	}

	void SceneManager::Move ()
	{
#if 0

		//シーン移項時
		P_Scene pScene = dynamic_pointer_cast <Scene> ( GameSceneManager::GetpScene () );
		if ( pScene->GetTransit () )
		{
			//フェード開始
			m_fade->SetBlackOut ( 60 );
		}
		//フェード中
		if ( m_fade->IsActive () )
		{
			m_fade->Move ();
			return;
		}


#endif // 0
		GameSceneManager::Move ();
	}

}	//namespace GAME

